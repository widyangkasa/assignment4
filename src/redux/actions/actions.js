import { signOut } from '../../screens/AuthScreens/SignOut';
import { clearPersistedUserAction, setToken } from './actionCreator';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';

export const logout = (navigation, uid) => {
    return async (dispatch)=>{
        try{
            navigation.replace("AuthStack");
            await signOut(uid);
            dispatch(clearPersistedUserAction());
            
        }catch (e) {
            navigation.replace("MainStack");
            alert(e.message);
        }
    }
}


export const refreshUserToken = (newToken,userDataRef) => {
    return async ()=>{
        try{
            await userDataRef.update({isLogin: true, token: newToken})
        }catch (e) {
            alert(e.message);
        }
    } 
}
