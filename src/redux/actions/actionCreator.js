import { PERSIST_USER_DATA, CLEAR_PERISISTED_USER, SET_DEVICE_TOKEN, UPDATE_USER_DATA } from '../../config/constants';

export const persistUserAction = (user) => {
    return {
        type: PERSIST_USER_DATA,
        data: {
            user
        }
    }
}

export const clearPersistedUserAction = () => {
    console.log("clear persisted user")
    return {
        type: CLEAR_PERISISTED_USER
    }
}

export const setToken = (token) => {
    return {
        type: SET_DEVICE_TOKEN,
        data: token
    }
}

export const updateUserData = (userData) => {
    // console.log("update user data")
    return {
        type: UPDATE_USER_DATA,
        data: userData
    }
}