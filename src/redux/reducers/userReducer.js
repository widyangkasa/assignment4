import { combineReducers } from 'redux';
import { PERSIST_USER_DATA, CLEAR_PERISISTED_USER, SET_DEVICE_TOKEN, UPDATE_USER_DATA } from '../../config/constants';
import messageReducer from '../../screens/MainScreens/redux/reducers/messageReducer';
import mPendingReducer from '../../screens/MainScreens/redux/reducers/mPendingReducer';
import roomReducer from '../../screens/MainScreens/redux/reducers/roomReducer';
import mRunningReducer from '../../screens/MainScreens/redux/reducers/mRunningReducer';
import mReceivedReducer from '../../screens/MainScreens/redux/reducers/mReceivedReducer';

const initialState = {
}

const userDataReducer = (state = initialState, action) => {
    // console.log("data in userdatareducer", {
    //     ...state,
    //     ...action.data
    // })
    switch(action.type){
        case PERSIST_USER_DATA:
            return {
                ...action.data.user
            }
        case CLEAR_PERISISTED_USER: 
            return initialState
        case UPDATE_USER_DATA:
            return {
                ...state,
                ...action.data
            }
            
        case SET_DEVICE_TOKEN:
            return {
                ...state,
                ...action.data
            }
        default: 
            return state;
    }
}

export const userReducer = combineReducers({
    data : userDataReducer,
    rooms : roomReducer ,
    messages: messageReducer,
    received: mReceivedReducer,
    pending: mPendingReducer,
    running: mRunningReducer
})
