export const chatStatusIcon = {
    pending: 'time-outline',
    sent: 'checkmark-outline',
    delivered: 'checkmark-done-outline',
    read: 'checkmark-done-outline'
}