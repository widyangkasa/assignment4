export const myColors = {
    primary: '#D01774',
    primaryLight: '#D24674',
    primarySuperLight: '#ffd3e9',
    primaryDark: '#842E83',
    secondary: '#292957',
    secondaryDark: '#232350',
    secondaryLight: '#4343FF',
    secondarySuperLight: '#dbdbff',
    default: 'white',
    defaultLight: '#D0D2DA',
    defaultDark:'#999997',
    defaultSuperDark: 'black'    
}

export const myFontSize = {
    superSmall: 10,
    small: 11,
    default: 14,
    big: 16,
    superBig: 18

    
}