import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { myColors } from '../config/myCss';

class ChatDate extends Component {
  

  render() {
    const {date} = this.props;
    return (
      <View style={styles.dateContainer}>
          <Text style={styles.dateText}>{date}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    dateContainer: {
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10
    },
    dateText: {
        color: myColors.defaultDark
    }
})

export default ChatDate;