import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';

class CProfilePic extends Component {
  

  render() {
    const {imagePath, diameter} = this.props;
    
    return (
        <Image source={{uri:imagePath!=''? imagePath:'../assets/userIconPicture.jpg'}}
        style={styles(diameter).profilePic}
        />
        
    )
  }
}

const styles = (diameter) =>  StyleSheet.create({
    profilePic: {
        width: diameter,
        height: diameter,
        borderRadius: diameter/2
    }
})

export default CProfilePic;