import React, {Component} from 'react';
import Icons from 'react-native-vector-icons/Ionicons';
import { myColors } from '../config/myCss';
import { chatStatusIcon } from '../config/others';

class CIconStatus extends Component{
    render() {
        const {messageStatus} = this.props;
        return(
            <Icons name={chatStatusIcon[messageStatus]}
                                    color={messageStatus==='read'?'#00FE67':myColors.default} 
                                />
            
        )
    }
}

export default CIconStatus;