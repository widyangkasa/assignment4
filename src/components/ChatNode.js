import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import { myColors, myFontSize } from '../config/myCss';
import { buildSpace } from '../utils/buildSpace';
import { chatStatusIcon } from '../config/others';
import ChatDate from './ChatDate';
import { isSameDateAsPrevMessages } from '../utils/viewMessages';
import { secondToDateBuilder } from '../utils/timeConverter';
import CIconStatus from './CIconStatus';

const {height, width} = Dimensions.get('window');

class ChatNode extends Component {
  

    render() {

        const {chatText, chatTime, isUser, chatStatus, prevMes, currentMes, index, chatDate} = this.props;
        // console.log(prevMes);
        // console.log(currentMes);
        const myStyle = styles(isUser);
        return (
        <View>
            {
                isSameDateAsPrevMessages(index, prevMes, currentMes)&&
                ( <ChatDate date={chatDate}/>)
               
            }
            <View style={myStyle.chatNodeContainer}>

                
                <View style={myStyle.chatNodeWrapper}>
                    <Text style={myStyle.chatText}>{chatText}{isUser?buildSpace(15):buildSpace(11)}</Text>
                    <Text style={myStyle.chatTime}>{chatStatus!=='pending'?chatTime:null}{buildSpace(2)}
                        {
                            isUser?(
                                <CIconStatus messageStatus = {chatStatus}/>
                            ):null
                        }
                    </Text>
                    {/* <Text>{chatTime}</Text> */}
                </View>
                
            </View>
        </View>
        )
  }
}

const styles = (isUser)=> StyleSheet.create({
    chatNodeContainer:{
        // backgroundColor: "green",
        padding: 5,
        flexDirection:'row',
        justifyContent: isUser?'flex-end': 'flex-start',
    },
    chatNodeWrapper: {
        backgroundColor: isUser?myColors.primary: myColors.secondary,
        minHeight: 25,
        minWidth: width*0.25,
        maxWidth: width*0.7,
        paddingTop: 5.5,
        paddingBottom: 5.5,
        paddingLeft: 7,
        paddingRight: 7,
        borderRadius: 5.5,
        shadowColor: myColors.defaultDark,
        shadowOffset: {
            width: 0,
	        height: 2,
        },
        elevation: 3,
        justifyContent: 'center',
        alignItems:'flex-start'
    },
    chatText:{
        lineHeight: 20,
        color: myColors.default,
        // backgroundColor:'orange',
        maxWidth: width*1,
        textAlign: 'left'
    },
    chatTime:{
        position: 'absolute',
        bottom: 0,
        right: 5.5,
        fontSize: myFontSize.superSmall,
        color: myColors.defaultLight
    }
})

export default ChatNode;