import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import HomeScreen from '../screens/MainScreens/HomeScreen';
import ChatListScreen from '../screens/MainScreens/ChatListScreen';
import ChatScreen from '../screens/MainScreens/ChatScreen';
import UserListScreen from '../screens/MainScreens/UserListScreen';
import LoginContainer from '../screens/AuthScreens/LoginScreen';
import RegisterContainer from '../screens/AuthScreens/RegisterScreen';
import CustomChatListHeader from '../screens/MainScreens/ChatListScreen/customChatListHeader';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

export const MainStack = ({name}) => {
    return (
      <Stack.Navigator initialRouteName="ChatApp">
        <Stack.Screen name="ChatApp"  component={TabStack} options={{title: `ChatApp, ${name}`}}/>
        <Stack.Screen name="ChatScreen" component={ChatScreen}/>
      </Stack.Navigator>
    );
  }


export const AuthStack = ()=> {
    return (
      <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
        <Stack.Screen name="Login" component={LoginContainer} />
        <Stack.Screen name="Register" component={RegisterContainer} />
      </Stack.Navigator>
    );
  }



export const TabStack = ()=> {
    return (
      <Tab.Navigator initialRouteName="Chat" headerMode='none'>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Chat" component={ChatListScreen}  options={{
          title: ()=> <CustomChatListHeader />
        }}/>
        <Tab.Screen name="Contact" component={UserListScreen} />
      </Tab.Navigator>
    );
  }

