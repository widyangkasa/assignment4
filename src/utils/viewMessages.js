import { secondToDateBuilder } from "./timeConverter";

export const isSameDateAsPrevMessages = (index, prev, current) => {
    // console.log("in date block")
    if (index ==0){
        return true;
    }else{
        // let prevDate = secondToDateBuilder(prev.createdAt.seconds);
        // let currentDate = secondToDateBuilder(current.createdAt.seconds);
        if((Math.trunc(current.createdAt.seconds/86400) - Math.trunc(prev.createdAt.seconds/86400))<1){
            return false;
        }else{
            return true;
        }
        
    }
}

