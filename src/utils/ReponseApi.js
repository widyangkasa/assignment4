class ResponseApi {
    constructor(data, message){
        this.data = data,
        this.message = message
    }

    getData=()=> {
        return this.data;
    }

    getMessage=()=> {
        return this.message;
    }

    setData=(data)=> {
        this.data = data;
        return this;
    }

    setMessage = (message) => {
        this.message = message;
        return this;
    }
}

export default ResponseApi;

// export const ResponseApi = ()=> {
//     let _data;
//     let _message;
//     return{
//         getData : () => _data,
//         getMessage : () => _message,
//         setData : (data) => _data = data,
//         setMessage : (message) => _message = message  
//     }    
// }