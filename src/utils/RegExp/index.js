export const Pattern = {
    email: /^[a-zA-z\d\.-]*@[\w-]*\.[a-zA-Z]{2,3}(\.[a-zA-Z])?$/,
    password: /^[\w@-]{6,20}$/,
    confirmPassword: /^[\w@-]{5,20}$/,
    userName: /^[a-zA-Z\d]{5,50}$/,
    fullName: /^[a-zA-Z\d ]{5,50}$/
    
}

export const errInput = {
    email: "plesae enter valid email address",
    password: "password must contains alphanumeric or digits min 6, max 20",
    confirmPassword: "confirm password not match",
    userName: "username must contains aphabets min 5, max 50",
    fullName: "name contains aphabets min 5, max 50"
}


export const isValidPattern = (text, property) => {
    let isValidPattern = Pattern[property].test(text);
    return isValidPattern;
}