export const secondToDateBuilder = (seconds) => {
    let miliseconds = seconds*1000;
    return `${new Date(miliseconds).getDate()}/${new Date(miliseconds).getMonth()+1}/${new Date(miliseconds).getFullYear()}`;
}


export const secondToTimeBuilder = (seconds) => {
    let miliseconds = seconds*1000;
    return new Date(miliseconds).toLocaleTimeString([], {hour:'2-digit', minute: '2-digit', hour12: false}).toString().slice(0,5)
}

export const convertLastMessageDateTime= (seconds) => {
    let now = new Date().getTime()/1000;
    if(now-seconds<(60*60*24)){
        return secondToTimeBuilder(seconds);
    }else if(now-seconds<(60*60*24*2)){
        return "Yesterday"
    }else {
        return secondToDateBuilder(seconds);
    }
}