export const generateRoomId = (senderUserId, receiverUserId) => {
    if (senderUserId< receiverUserId){
        return `${senderUserId}${receiverUserId}`;
    }else {
        return `${receiverUserId}${senderUserId}`;
    }
}

export const findReceiverIdbyRoomId = (roomId, myId) => {
    var modifiers = "g"
    var patt = new RegExp(myId,modifiers);
    const userIdArray = roomId.split(patt);
    let receiverId = '';
    for (let i =0; i<2; i++){
        if(userIdArray[i] !== ''){
            receiverId= userIdArray[i]
            break;
        }            
    }
    return receiverId;
}