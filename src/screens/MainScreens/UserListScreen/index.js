import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import CProfilePic from '../../../components/CProfilePic';
import { generateRoomId } from '../../../utils/generateId';
import { connect } from 'react-redux';
import { myColors, myFontSize } from '../../../config/myCss';

const {height, width} = Dimensions.get('window');
class UserListScreen extends Component {
  
  unsunscribeUserList;
  state = {
    userList: []
  }
  async componentDidMount(){
    this.unsunscribeUserList = firestore().collection('user').where('id', '!=', this.props.myId).onSnapshot((snapshots)=> {
        let users = []
        snapshots && snapshots.forEach((user)=>
          users.push(user.data())
        )
        this.setState({
          userList: users
        })
    });
  }

  componentWillUnmount(){
    this.unsunscribeUserList && this.unsunscribeUserList();
  }

  goToChatScreen = (userId) => {
    let roomId = generateRoomId(userId, this.props.myId)
    this.props.navigation.navigate("ChatScreen", {'roomId': roomId, 'userId': userId})
  }

  render() {

    return (
      <View style={styles.screenContainer}>
        <ScrollView>
        {
          this.state.userList.map((user, index)=> (

            <View key = {user.id} >
                  <View style={styles.profilePicContainer}>
                    <CProfilePic imagePath = {user.profilePic}
                        diameter={60}
                    />
                  </View>
                  <TouchableOpacity onPress= {()=> this.goToChatScreen(user.id)} style={styles.userContainer}>
                      <View style={styles.profilePicContainerSpace}></View>
                      <View style={styles.userDetailsContainer}>
                        <View style={styles.topDetails}>
                          <Text style={styles.userName}>{user.name}</Text>
                        </View>
                      </View>
                  </TouchableOpacity>
            </View>
            
          ))
        }
        {/* <Text>{JSON.stringify(this.state)}</Text> */}
        </ScrollView>
          
      </View>
    )
  }
}

const styles = StyleSheet.create({
  screenContainer:{
    backgroundColor: myColors.secondaryDark,
    flex: 1
  },
  userContainer:{
    height: 88,
    // backgroundColor: 'green',
    flexDirection: 'row',
  },
  profilePicContainer:{
    position:'absolute',
    top: 0,
    height: 88,
    width: width*0.25,
    // backgroundColor: 'yellow',
    justifyContent:'center',
    alignItems:'center'
  },
  profilePicContainerSpace:{
    height: 88,
    width: width*0.25,
    // backgroundColor: 'yellow',
    justifyContent:'center',
    alignItems:'center'
  },
  userDetailsContainer: {
    // backgroundColor: 'blue',
    width: width*0.72,
    padding: 10,
    justifyContent: 'center',
    borderTopColor: myColors.secondaryLight,
    borderTopWidth: 0.2,
  },
  topDetails:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: 'orange',
    alignItems: 'center'
  },
  userName:{
    fontSize: myFontSize.big,
    fontWeight: 'bold',
    paddingBottom: 5,
    color: myColors.default
  },
})


const mapStateToProps= (state) => {
  return{
    myId: state.user.data.id
  }
}

export default connect(mapStateToProps, null)(UserListScreen);