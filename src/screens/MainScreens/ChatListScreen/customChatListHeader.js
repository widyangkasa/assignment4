import React, { Component } from 'react';
import { View, Text, StyleSheet} from 'react-native';
import { myColors, myFontSize } from '../../../config/myCss';
import { connect } from 'react-redux';

class CustomChatListHeader extends Component {

    state = {
        count: 0
    }

    _countUnreadRoom = () => {
        let count= 0
        Object.keys(this.props.received).forEach((key, index)=> {
            Object.keys(this.props.received[key]).length >0? count +=1: null 
        })
        return count;
    }


    render() {
        return (
        <View style={styles.tabContainer} >
            <Text>CHAT</Text>
            {
            this._countUnreadRoom() >0? (
                <View style={styles.chatCount}>
                    <Text style={{color: 'white', fontSize: myFontSize.small}}>{this._countUnreadRoom()}</Text>
                </View>
            ):null 
            
            }
        </View>

        )
    }
}

const styles = StyleSheet.create({
    tabContainer : {
        flexDirection: 'row',
        alignItems:'center'
    },
    chatCount: {
        backgroundColor: myColors.primary,
        minWidth: 25,
        height: 25,
        borderRadius: 25/2,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10
    }
})

const mapStateToProps = (state) => {
    return {
        received: state.user.received
    }
}

export default connect(mapStateToProps)(CustomChatListHeader);