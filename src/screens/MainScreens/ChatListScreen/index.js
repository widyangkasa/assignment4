import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import CProfilePic from '../../../components/CProfilePic';
import { myFontSize, myColors } from '../../../config/myCss';
import { convertLastMessageDateTime } from '../../../utils/timeConverter';
import { connect } from 'react-redux';
import CIconStatus from '../../../components/CIconStatus';
import { buildSpace } from '../../../utils/buildSpace';

const {height, width} = Dimensions.get('window');

class ChatListScreen extends Component {

  //return lastMessage Detail from messagesObject of a particular room 
  _findLastMessage =(messagesInRoom) => {
    let lastId = '';
    const messagesArray = Object.values(messagesInRoom);
    messagesArray.map((message, index)=> {
      if(index == 0){
        lastId = messagesArray[index].id
      }else if( messagesArray[index-1].createdAt.seconds < message.createdAt.seconds){
        lastId = message.id
      }

    })
    return messagesInRoom[lastId];
  }

  _sortRooms = (messages) => {
    const sortedRoomsArray = [];
    //check if there are rooms in Redux-messages
    const roomsIdArray = Object.keys(messages);
    
    if(roomsIdArray.length>0){
      //iterate every room
      roomsIdArray.map((roomId, index) => {
        const messagesIdsArray = Object.keys(roomId)
        //check if there are messages in the room. if exist, then find lastMessage
        if(messagesIdsArray.length>0){
          const lastMessage = this._findLastMessage(messages[roomId]);
          const unRead = this.props.received[roomId] ? Object.keys(this.props.received[roomId]).length: 0;
          sortedRoomsArray.push({roomId: roomId, lastMessage: lastMessage, unRead: unRead})
        }
      })
    }
    if(sortedRoomsArray.length>0){
      sortedRoomsArray.sort((a,b)=> b.lastMessage.createdAt.seconds - a.lastMessage.createdAt.seconds)
    }
    return sortedRoomsArray;
  }

  render() {

    const rooms = this.props.rooms;
    const messages = this.props.messages;
    const sortedRooms = this._sortRooms(messages);

    return (
      <View style={styles.screenContainer}>
          <ScrollView>

            {Object.keys(rooms).length>0 && sortedRooms.length>0?
              sortedRooms.map((room, index)=>{
                return (

                  <View key = {index}>
                    <View style={styles.profilePicContainer}>
                      <CProfilePic imagePath = {rooms[room.roomId].withUser? rooms[room.roomId].withUser.profilePic: ''}
                          diameter={60}
                      />
                    </View>
                    <TouchableOpacity
                        onPress={()=>this.props.navigation.navigate("ChatScreen", {'roomId': room.roomId, 'userId': rooms[room.roomId].withUserId})}
                        style={styles.roomContainer}
                        onLongPress={()=> alert("long press")}
                      >
                        <View style={styles.profilePicContainerSpace}></View>
                        <View style={styles.roomDetailsContainer}>
                          <View style={styles.roomTopDetails}>
                            <Text style={styles.roomUserName}>{rooms[room.roomId].withUser? rooms[room.roomId].withUser.name: 'ChatApp user'}</Text>
                            <Text style={[styles.roomDateTime, room.unRead>0?{color: myColors.primary}:null]}>{convertLastMessageDateTime(room.lastMessage.createdAt.seconds)}</Text>
                          </View>
                          <View style={styles.roomBottomDetails}>
                              {
                                room.lastMessage.sender==this.props.userId?
                                (
                                  <Text numberOfLines={1}  style={styles.roomLastMessage}>
                                    <CIconStatus messageStatus={room.lastMessage.status}/>
                                    {buildSpace(3)}
                                    {room.lastMessage.content}
                                  </Text>
                                ):
                                (
                                  <Text numberOfLines={1} style={styles.roomLastMessage}>
                                    {room.lastMessage.content}
                                  </Text>
                                )
                              }
                              {
                                room.unRead>0?(
                                  <View style ={styles.unReadContainer}>
                                    <Text style={{color: 'white', fontSize: myFontSize.small}} numberOfLines={1}>{room.unRead>99?('99+'):(room.unRead) }</Text>
                                  </View>
                                ):null
                              }
                          </View>
                        </View>
                    </TouchableOpacity>
                  </View>
                )
              }):null}
            
            {/* <Text style={{color: 'white'}}>{JSON.stringify(this.props.rooms)}</Text> */}
          </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  screenContainer:{
    backgroundColor: myColors.secondaryDark,
    flex: 1
  },
  roomContainer:{
    height: 88,
    // backgroundColor: 'green',
    flexDirection: 'row',
  },
  profilePicContainer:{
    position:'absolute',
    top: 0,
    height: 88,
    width: width*0.25,
    // backgroundColor: 'yellow',
    justifyContent:'center',
    alignItems:'center',
  },
  unReadContainer:{
    backgroundColor: myColors.primary,
    minWidth: 25,
    height: 25,
    borderRadius: 25/2,
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
    padding: 5,
    maxWidth: 38,
    fontWeight: 'bold'
  },
  profilePicContainerSpace:{
    height: 88,
    width: width*0.25,
    // backgroundColor: 'yellow',
    justifyContent:'center',
    alignItems:'center'
  },
  roomDetailsContainer: {
    // backgroundColor: 'blue',
    width: width*0.72,
    padding: 10,
    justifyContent: 'center',
    borderTopColor: myColors.secondaryLight,
    borderTopWidth: 0.2,
  },
  roomTopDetails:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: 'orange',
    alignItems: 'center'
  },
  roomBottomDetails: {
    // backgroundColor: 'green',
    height: 25,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },
  roomUserName:{
    fontSize: myFontSize.big,
    fontWeight: 'bold',
    paddingBottom: 5,
    color: myColors.default
  },
  roomDateTime:{
    color: myColors.secondarySuperLight,
    fontSize: myFontSize.small,
    fontWeight: 'bold'
  },
  roomLastMessage:{
    color: myColors.secondarySuperLight
  }
})

const mapStateToProps = (state) => {
  return {
    userId : state.user.data.id,
    rooms: state.user.rooms,
    messages: state.user.messages,
    received: state.user.received
  }
}


export default connect(mapStateToProps, null)(ChatListScreen);