
import React, { Component } from 'react';
import { AppState } from 'react-native';
import { MainStack } from '../../Navigator';
import { connect } from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';
import { updateUserData } from '../../redux/actions/actionCreator';
import { refreshUserToken } from '../../redux/actions/actions';
import { addRoom, updateRoomData } from './redux/actions/roomAction';
import { updateMessage } from './redux/actions/messageAction';
import { sendMessageToDB } from './sendMessage';
import { addRunning } from './redux/actions/mRunningAction';
import { setPendingNotInUseAction } from './redux/actions/mPendingActionCreator';
import { setStatusDelivered, setStatusRead } from './redux/actions/messageActionCreator';
import { addReceived } from './redux/actions/mReceivedAction';
import { sendTask } from './sendTask';

class MainStackContainer extends Component {
    state = {
        appState: AppState.currentState
      };
    
    userPublicRef = firestore().collection('user');
    unsubscribePublicUserData;
    unsubscribeOnTokenRefreshListener;

    unsubscribeRoomListener;
    unsubscribeUpdateOnline;

    unsubscribeMessageListener;
    unsubscribePendingMessage;
    unsubscribeTasksListener;
    async componentDidMount(){

        //app state change listener
        AppState.addEventListener("change", this._handleAppStateChange);


        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
              'Notification caused app to open from background state:',
              remoteMessage.data,
            );
            this.props.navigation.navigate("ChatScreen", {'roomId': remoteMessage.data.roomId, 'userId': remoteMessage.data.userId})
          });
      
        this.props.setPendingNotInUse();
        this._sendRunning();
        
        //capture user details update on db and sync with storage
        this.unsubscribePublicUserData =this.userPublicRef.doc(this.props.user.id).onSnapshot((snapShot)=> {
            snapShot &&
            // console.log("snapshot")
                // console.log(snapShot.data())
                this.props.updateUserDataInStorage(snapShot.data());
            })

        //update token in DB
        this.unsubscribeOnTokenRefreshListener = messaging().onTokenRefresh(token => {
            this.props.refreshUserTokenInDB(token, userPublicRef)
        })

        //reference to user chats rooms
        const tasksCollectionRef = this.userPublicRef.doc(this.props.user.id).collection('tasks');

        
        //listen to user chats/task, if task exist, do task()
        this.unsubscribeTasksListener = tasksCollectionRef.onSnapshot((tasksSnapShots) => {
            console.log("in task listener")
            if(tasksSnapShots){
                console.log("found task snapshots")
                console.log('tasksSnapShots', tasksSnapShots.docs)
                tasksSnapShots.forEach((taskMesssage) => {
                    const task = taskMesssage.data();
                    console.log("task", task);
                    const messageId = taskMesssage.id;
                    const roomId = task.roomId;
                    if(this.props.messages[roomId]){
                        if(this.props.messages[roomId][messageId]){
                            console.log(task.value);
                            if(task.value =='delivered'){
                                this.props.setMessageStatusDelivered(messageId, roomId);
                                tasksCollectionRef.doc(messageId).delete();
                            }else if(task.value =='read'){
                                console.log("case read")
                                this.props.setMessageStatusRead(messageId, roomId);
                                tasksCollectionRef.doc(messageId).delete();
                            }
                        }else{
                            tasksCollectionRef.doc(messageId).delete();
                        }
                    }
                })
            }
        })



        //capture /messages and sync with storage messages. for each snapshot, update the status to delivered if senderid != userid
        this.unsubscribeMessageListener = firestore().collection("rooms").where('owner', 'array-contains', this.props.user.id).onSnapshot((roomSnapshots)=> {
            // console.log("in message listener", roomSnapshots)
                    if(roomSnapshots && !roomSnapshots.empty){
                        // console.log("found room snapshots")
                        roomSnapshots.forEach((roomSnapshot)=>{
                            const roomData = roomSnapshot.data();
                            // console.log("room snapshots", roomData)
                            const roomId = roomSnapshot.id;
                            if(!this.props.rooms[roomId]){
                                // console.log("roomId not exist")
                                roomData.owner.forEach((id) =>{
                                    if(id!=this.props.user.id){
                                        this.userPublicRef.doc(id).get().then((withUserData)=> {
                                            roomData['withUserId'] = id;
                                            roomData['withUser'] = withUserData && withUserData.data();
                                            console.log("room Data", roomData)
                                            this.props.addRoomDataInStorage(roomId, roomData);
                                        })
                                    }
                                })
                            }else{
                                this.props.updateRoomDataInStorage(roomData);
                            }
                            firestore().collection('rooms').doc(roomId).collection('messages').onSnapshot((msgSnapShots)=> {
                                if(msgSnapShots && !msgSnapShots.empty){
                                    msgSnapShots.forEach((msg)=> {
                                        console.log("found new message")
                                        // console.log(roomSnapshot.id);
                                        // console.log(msg.data(), roomSnapshot.id)

                                        const messageData = msg.data();
                                        messageData['id'] = msg.id;
                                        
                                        this.props.addAndUpdateMessageInStorage(messageData, roomId );
                                        if(messageData.sender!= this.props.user.id ){
                                            this.props.addReceivedInStorage(roomId, msg.id)
                                           if(messageData.status== 'sent'){
                                                const taskdoc = {};
                                                taskdoc['messageId'] = msg.id
                                                var batch = firestore().batch();
                                                batch.set(this.userPublicRef.doc(messageData.sender).collection('tasks').doc(msg.id),
                                                {roomId: roomId, messageId: msg.id,  action: 'set', property: 'status', value: 'delivered'}, {merge: true});
                                                batch.delete(firestore().collection("rooms").doc(roomId).collection('messages').doc(msg.id));
        
                                                batch.commit();
                                           }
                                        //    else if (messageData.status== 'sent'){
                                        //         firestore().collection('rooms').doc(roomId).collection('messages').doc(messageData.id).update({status: 'delivered'});
                                        //    }
                                            
                                        }
                                    })
                                } 
                            })
                        })
                    }
                    
                }) 
        
        //listen to pending message
        this.unsubscribePendingMessage = setInterval(()=> {
            if(this.props.pending.inUse == false){
                if(Object.keys(this.props.pending.data).length >0){
                    Object.keys(this.props.pending.data).map((key, index)=> {
                        const messageId = key;
                        const actionData = this.props.pending.data[key];
                        console.log("found pending message, ", messageId, "to room", actionData)
                        console.log("pending", this.props.pending.data)
                        console.log("running", this.props.running)
                        this.props.movePendingToRunning(messageId, actionData)
                        this._sendRunning();
                    })
                }      
            }
                  
        }, 200)
    }

    componentWillUnmount(){
        console.log("main stack unmount")
        AppState.removeEventListener("change", this._handleAppStateChange);
        this.unsubscribePublicUserData && this.unsubscribePublicUserData();
        this.unsubscribeOnTokenRefreshListener && this.unsubscribeOnTokenRefreshListener();
        this.unsubscribeTasksListener && this.unsubscribeTasksListener();
        this.unsubscribeMessageListener && this.unsubscribeMessageListener();
        clearInterval(this.unsubscribeUpdateOnline);
    }


    _sendRunning = () => {
        Object.keys(this.props.running).map((key, index)=> {
            const messageId = key;
            const actionData = this.props.running[key];
            if(actionData.action == 'SEND_MESSAGE'){
                console.log("send message")
                const messageData = this.props.messages[actionData.roomId][messageId];
                this.props.doSendMessageToDB(messageData, actionData.roomId, messageId);
            }else if (actionData.action == 'SEND_TASK'){
                console.log("send task")
                const taskDetail = actionData.detail;
                this.props.doSendTaskToDB(taskDetail);
            }

        })
    }

    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
          console.log("App has come to the foreground!");
          this.userPublicRef.doc(this.props.user.id).update({isOnline: true});
        } else{
            console.log("App has come to the background!");
            this.userPublicRef.doc(this.props.user.id).update({isOnline: false,lastOnline: firestore.FieldValue.serverTimestamp()});
        }
        this.setState({ appState: nextAppState });
      };


    
    render() {
        return (
            <MainStack name = {this.props.user.name}/>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
        rooms: state.user.rooms,
        messages: state.user.messages,
        pending: state.user.pending,
        running: state.user.running
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateUserDataInStorage : (userData) => dispatch(updateUserData(userData)),
        refreshUserTokenInDB : (token, userRef) => dispatch(refreshUserToken(token, userRef)),
        updateRoomDataInStorage: (room) => dispatch(updateRoomData(room)),
        addRoomDataInStorage: (roomId,room) => dispatch(addRoom(roomId, room)),
        addAndUpdateMessageInStorage: (messageData, roomId) => dispatch(updateMessage(messageData, roomId)),
        addReceivedInStorage: (roomId, messageId) => dispatch(addReceived(roomId, messageId)),
        movePendingToRunning: (messageData, actionData) => dispatch(addRunning(messageData, actionData)),
        setPendingNotInUse: ()=> dispatch(setPendingNotInUseAction()),
        setMessageStatusDelivered : (messageId, roomId) => dispatch(setStatusDelivered(messageId, roomId)),
        setMessageStatusRead: (messageId, roomId) => dispatch(setStatusRead(messageId, roomId)),
        doSendMessageToDB : (messageData, roomId, messageId) => dispatch(sendMessageToDB(messageData, roomId, messageId)),
        doSendTaskToDB : (taskDetail) => dispatch(sendTask(taskDetail))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MainStackContainer);