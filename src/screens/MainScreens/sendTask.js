import { addPendingTask } from "./redux/actions/mPendingAction";
import { removeRunning } from "./redux/actions/mRunningAction";
import firestore from '@react-native-firebase/firestore';

export const sendTask = (taskDetail) => {

    return async (dispatch, getState) => {
        try{
            await firestore().collection('user').doc(taskDetail.userId).collection('tasks').doc(taskDetail.messageId)
            .set(taskDetail);
            dispatch(removeRunning(taskDetail.messageId))
        }catch(e){
            dispatch(removeRunning(taskDetail.messageId))
            dispatch(addPendingTask(taskDetail.messageId, taskDetail))
        }
    }
    
}