import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CProfilePic from '../../../components/CProfilePic';
import { myFontSize } from '../../../config/myCss';
import { convertLastMessageDateTime } from '../../../utils/timeConverter';
import firestore from '@react-native-firebase/firestore';

class CustomChatHeader extends Component {

  state={
    receiverData: {
      id: '',
      name: '',
      profilePic: '',
      isOnline: false,
      lastOnline: {seconds: 0}
    }
  }

  interval;
  unsubscribeReceiverData;

  componentDidMount() {
    this.unsubscribeReceiverData=firestore().collection('user').doc(this.props.route.params.userId).onSnapshot((snapShot)=> {
        // console.log(snapShot.data());
        snapShot && 
          this.setState({
            ...this.state,
            receiverData : snapShot.data()
          })

      }) 
  }

  componentWillUnmount() {
    this.unsubscribeReceiverData && this.unsubscribeReceiverData();
    clearInterval(this.interval);
  }

  lastOnline = ()=>{
    return convertLastMessageDateTime(this.state.receiverData.lastOnline.seconds)
  }

  render() {

    const {name, profilePic, isOnline} = this.state.receiverData;
    // console.log("state in header", this.state.receiverData);
    const status = isOnline?'Online':`Last online ${this.lastOnline()}`;
    return (
      <View style={styles.chatHeaderContainer}>
        <View>
          {/* <Text>Hello,{this.props.route.params.title} </Text> */}
          <CProfilePic imagePath = {profilePic}
            diameter={50}
          />
        </View>
        <View style={styles.headerDetails}>
          <Text style={styles.headerName}>{name}</Text>
          <Text>{
            status
          }</Text>
          {/* <Text>{JSON.stringify(this.state)}</Text> */}
        </View>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  chatHeaderContainer:{
    flexDirection:'row',
    // backgroundColor:'green'
  },  
  headerDetails:{
    justifyContent: 'space-evenly',
    marginLeft: 10
  },
  headerName: {
    fontSize: myFontSize.big,
    fontWeight: 'bold'
  }
})

export default CustomChatHeader;