import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Dimensions, Keyboard, AppState } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import CustomChatHeader from './customChatHeader';
import { myColors } from '../../../config/myCss';
import Icons from 'react-native-vector-icons/Ionicons';
import ChatNode from '../../../components/ChatNode';
import { connect } from 'react-redux';
import { secondToDateBuilder, secondToTimeBuilder } from '../../../utils/timeConverter';
import { moveReceivedToPending, persistSendMessageInStorage } from '../redux/actions/messageAction';
import firestore from '@react-native-firebase/firestore';

const {height, width} = Dimensions.get('window');

class ChatScreen extends Component {
    state={
        receiverData: {
          id: '',
          name: '',
          profilePic: '',
          lastOnline: {seconds: 0}
        },
        receiverId: '',
        roomId: '',
        chatTextInput : '',
        isValidInput: false,
        appState: AppState.currentState
      }

    unsubscribeReceiverData;
    setReadInterval;
    scrollView = null;

    async componentDidMount(){

        console.log("did mount in chatscreen triggered")

        AppState.addEventListener("change", this._handleAppStateChange)

        this.keyboardDidShowListener =Keyboard.addListener('keyboardDidShow', this._scrollDown);

        this.props.navigation.setOptions({
            headerTitle: () => (
                <CustomChatHeader {...this.props} />
            ),
        });

        this.setState({
            receiverId: this.props.route.params.userId,
            roomId: this.props.route.params.roomId,
            chatTextInput: ''
        })

        this.unsubscribeReceiverData= firestore().collection('user').doc(this.props.route.params.userId).onSnapshot((snapShot)=> {
            if (snapShot.exists){
              this.setState({
                ...this.state,
                receiverData : snapShot.data()
              })
            }
          }) 

        this.setReadInterval = this._checkReceivedMessage();
    }

    _checkReceivedMessage = () => {
        return setInterval(()=> {
            // console.log("set read Interval")
            if(this.props.received && Object.keys(this.props.received).length >0){
                Object.values(this.props.received).forEach((message)=>{
                    const taskDetail = {roomId: this.props.route.params.roomId, messageId: message.id,  action: 'set', property: 'status', value: 'read', userId:this.props.route.params.userId };
                    this.props.handleReceived(taskDetail) 
                })
            } 
        }, 200)
    }

    componentWillUnmount(){
        this.keyboardDidShowListener.remove();
        this.unsubscribeReceiverData && this.unsubscribeReceiverData();
        clearInterval(this.setReadInterval);
    }

    _handleAppStateChange = nextAppState => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
          console.log("App has come to the foreground!");
          this.setReadInterval = this._checkReceivedMessage();
        } else{
            console.log("App has come to the background!");
            clearInterval(this.setReadInterval);
        }
        this.setState({ ...this.state, appState: nextAppState });
      };

    _scrollDown =()=> {
        this.scrollView.scrollToEnd({animated: false});
    }

    handleChangeText =(text) => {
        this.setState({
            ...this.state,
            chatTextInput : text,
            isValidInput: /^\s+$/.test(text) ==true? false: true 
        })
    }

    sendMessage = () => {
        const messageData = this.getMessageData();
        const roomData = this.getRoomData(messageData);
        this.props.persistSendMessage(messageData,this.props.route.params.roomId, roomData);
        this.setState({
            ...this.state,
            chatTextInput: ''
        })
    }

    getMessageData = () => {
        const date = new Date().getTime()/1000;
        return {
            status: 'pending',
            content: this.state.chatTextInput,
            type: 'text',
            sender: this.props.myId,
            createdAt: {'seconds': `${parseInt(date)}`},
            id: `${this.props.myId}${new Date().getTime()}`
        }
    }

    getRoomData = (messageData) => {
        return {
            owner : [this.props.myId, this.state.receiverData.id],
            [this.props.myId]: {
                'isTyping' : false,
                'unRead': 0
            },
            [this.state.receiverData.id]: {
                'isTyping' : false,
                'unRead': 1
            },
            withUserId: this.state.receiverId,
            lastMessage: {
                id: messageData.id
            },
            roomId: this.state.roomId,
            withUser: this.state.receiverData
        }
    }

  render() {
    const ar = [];
        for(let a=1; a<100; a++){
            ar.push(a);
        }
    const messages = this.props.messages;

    const messagesKey = Object.keys(messages).length>1?
                        Object.keys(messages).sort((a,b)=> messages[a].createdAt.seconds - messages[b].createdAt.seconds)
                        : Object.keys(messages);

    return (
        
      <View style={{ flex: 1, backgroundColor: myColors.default }}>
          <View style={{ flex: 1 }}>
            <ScrollView
                ref={(scroll) => {this.scrollView = scroll;}}
                onContentSizeChange={() => this._scrollDown()}
                >
                    <View style={styles.scrollViewStyle}>
                    {/* <Text>{JSON.stringify(this.state)}</Text> */}
                    {/* <Text>{JSON.stringify(this.props.messages)}</Text> */}
                    {/* <Text>{JSON.stringify(this.props.pending)}</Text> */}
                    {/* <Text>{this.state.chatTextInput}</Text> */}
                    { messagesKey.length!=0? 
                        messagesKey.map((key, index)=>(
                            <ChatNode key = {key} isUser={messages[key].sender == this.props.myId? true: false}
                                chatText={messages[key].content}
                                chatTime={secondToTimeBuilder(messages[key].createdAt.seconds)}
                                chatDate={secondToDateBuilder(messages[key].createdAt.seconds)}
                                chatStatus={messages[key].status}
                                index={index}
                                prevMes = {index!=0?messages[messagesKey[index-1]]:0}
                                currentMes = {messages[key]}
                         />
                        )):null
                    }
                    </View>
                </ScrollView>
            </View>
            <View style={styles.chatInputWrapper}>
    
                <TextInput style={styles.chatInput} multiline={true} onChangeText={(text)=> this.handleChangeText(text)}
                value={this.state.chatTextInput}
                />
                <View style={styles.sendButtonWrapper}>
                    
                        {
                            this.state.chatTextInput!= '' && this.state.isValidInput? (
                                <TouchableOpacity style={styles.sendButton} onPress={()=> this.sendMessage()}>
                                    <Icons name="send-outline" size={20} color={myColors.secondary}/>
                                </TouchableOpacity>
                            ):null
                        }


                </View>
            </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    scrollViewStyle: {
        // backgroundColor: 'green',
        minHeight: height*0.8,
        justifyContent: 'flex-end'
    },
    chatInputWrapper:{
        maxHeight: height*0.2,
        width: width,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        // backgroundColor: myColors.primaryLight,
        flexDirection: 'row',
        marginTop: 5,
        borderTopWidth:1,
        borderTopColor: myColors.defaultLight
    },
    chatInput : {
        // borderColor:myColors.primaryLight, 
        // borderWidth: 1,    
        color: myColors.defaultSuperDark,
        width: width*0.8,
        paddingLeft: 10
    },
    sendButtonWrapper:{
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        padding: 7,
        height: 50
    }
})

const mapStateToProps = (state, ownProps) => {
    return {
      messages : state.user.messages[ownProps.route.params.roomId] !== undefined? state.user.messages[ownProps.route.params.roomId]: {},
      myRoom: state.user.rooms[ownProps.route.params.roomId],
      myId: state.user.data.id,
      pending: state.user.pending,
      received: state.user.received[ownProps.route.params.roomId]
    }
  }


const mapDispatchToProps = (dispatch) => {
    return {
        handleReceived: (taskDetail) => dispatch(moveReceivedToPending(taskDetail)),
        persistSendMessage: (messageData, roomId, roomData) => dispatch(persistSendMessageInStorage(messageData, roomId, roomData)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);