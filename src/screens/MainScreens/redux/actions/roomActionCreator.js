import { ADD_ROOM, UPDATE_ROOM } from "../../../../config/constants"

export const updateRoomAction = (room) => {
    return {
        type: UPDATE_ROOM,
        data: room
    }
}
export const addRoomAction = (roomId, roomData) => {
    return {
        type: ADD_ROOM,
        data: {
            roomId,
            roomData
        }
    }
}