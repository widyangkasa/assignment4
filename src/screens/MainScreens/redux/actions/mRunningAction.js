
import { removePendingAction, setPendingInUseAction, setPendingNotInUseAction } from "./mPendingActionCreator"
import { addRunningAction, removeRunningAction } from "./mRunningActionCreator";

export const addRunning = (messageId, actionData) => {
    return (dispatch) => {
        dispatch(setPendingInUseAction());
        dispatch(addRunningAction(messageId, actionData))
        dispatch(removePendingAction(messageId))
        dispatch(setPendingNotInUseAction());
    }
}

export const removeRunning = (messageId) => {
    return (dispatch) => {
        dispatch(removeRunningAction(messageId))
    }
}

