import { addRoomAction, updateRoomAction } from "./roomActionCreator"

export const updateRoomData = (room) => {
    // console.log("update room data")
    return (dispatch) => {
        dispatch(updateRoomAction(room))
    }
}

export const addRoom = (roomId, roomData)=> {
    return(dispatch) => {
        dispatch(addRoomAction(roomId, roomData))
    }
}