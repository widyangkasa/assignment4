import { ADD_RECEIVED, REMOVE_RECEIVED } from "../../../../config/constants"

export const addReceivedAction = (roomId, messageId) => {
    return {
        type: ADD_RECEIVED,
        data:{
            roomId,
            messageId
        }
    }
}

export const removeReceivedAction = (roomId, messageId) => {
    return {
        type: REMOVE_RECEIVED,
        data:{
            roomId,
            messageId
        }
    }
}