import { ADD_RECEIVED, REMOVE_RECEIVED } from "../../../../config/constants"
import { addReceivedAction, removeReceivedAction } from "./mReceivedActionCreator"

export const addReceived = (roomId, messageId) => {
    return (dispatch) => {
        dispatch(addReceivedAction(roomId, messageId));
    }
}

export const removeReceived = (roomId, messageId) => {
    return (dispatch) => {
        dispatch(removeReceivedAction(roomId, messageId));
    }
}