import { addMessageAction, setStatusRead, updateMessageAction } from "./messageActionCreator";
import { addPendingMessage, addPendingTask } from "./mPendingAction";
import { addReceived, removeReceived } from "./mReceivedAction";
import { addRoom, updateRoomData } from "./roomAction";

export const updateMessage = (messageData, roomId) => {
    return (dispatch) => {
        dispatch(updateMessageAction(messageData, roomId))
    }
}

export const persistSendMessageInStorage = (messageData, roomId, roomData) => {
    return (dispatch, getState) => {
        dispatch(addMessageAction(messageData, roomId))
        dispatch(addPendingMessage(messageData, roomId))
        if (getState().user.rooms[roomId]){
            dispatch(updateRoomData(roomData))
        }else{
            dispatch(addRoom(roomId, roomData))
        }
        
    }
}

export const moveReceivedToPending = (taskDetail) => {
    return (dispatch, getState) => {
        console.log("move received")
        try{
            dispatch(setStatusRead(taskDetail.messageId, taskDetail.roomId))
            dispatch(addPendingTask(taskDetail.messageId, taskDetail))
            dispatch(removeReceived(taskDetail.roomId, taskDetail.messageId));
        }catch (e){
            dispatch(addReceived(taskDetail.roomId, taskDetail.messageId))
        }
        
    }
}