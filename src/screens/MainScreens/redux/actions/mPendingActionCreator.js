import { ADD_PENDING, REMOVE_PENDING, SET_PENDING_INUSE, SET_PENDING_NOTINUSE } from "../../../../config/constants"


export const addPendingAction = (messageId, actionData) => {
    return {
        type: ADD_PENDING,
        data: {
            messageId,
            actionData
        }
    }
}

export const removePendingAction = (messageId) => {
    return {
        type: REMOVE_PENDING,
        data: {
            messageId
        }
    }
}

export const setPendingInUseAction = () => {
    return {
        type: SET_PENDING_INUSE
    }
}

export const setPendingNotInUseAction = () => {
    return {
        type: SET_PENDING_NOTINUSE
    }
}



