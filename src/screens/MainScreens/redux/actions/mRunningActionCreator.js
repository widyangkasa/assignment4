import { ADD_RUNNING, REMOVE_RUNNING } from "../../../../config/constants"


export const addRunningAction = (messageId, actionData) => {
    return {
        type: ADD_RUNNING,
        data: {
            messageId,
            actionData
        }
    }
}

export const removeRunningAction = (messageId) => {
    return {
        type: REMOVE_RUNNING,
        data: {
            messageId
        }
    }
}