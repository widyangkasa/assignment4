import { ADD_MESSAGE, SET_STATUS_DELIVERED, UPDATE_MESSAGE,  SET_STATUS_READ } from "../../../../config/constants"

export const updateMessageAction = (messageData, roomId) => {
    return{
        type: UPDATE_MESSAGE,
        data: {
            messageData,
            roomId
        }
    }
}

export const addMessageAction = (messageData, roomId) => {
    return{
        type: ADD_MESSAGE,
        data: {
            messageData,
            roomId
        }
    }
}

export const setStatusDelivered = (messageId, roomId) => {
    return{
        type: SET_STATUS_DELIVERED,
        data: {
            messageId,
            roomId
        }
    }
}

export const setStatusRead = (messageId, roomId) => {
    console.log("set read status")
    return{
        type: SET_STATUS_READ,
        data: {
            messageId,
            roomId
        }
    }
}
