import { addPendingAction, removePendingAction } from "./mPendingActionCreator"

export const addPendingMessage = (messageData, roomId) => {
    return (dispatch) => {
        dispatch(addPendingAction(messageData.id, {action: 'SEND_MESSAGE', roomId: roomId}))
    }
}

export const addPendingTask = (messageId, taskDetail) => {
    return (dispatch) => {
        dispatch(addPendingAction(messageId, {action: 'SEND_TASK', detail: taskDetail}))
    }
}


export const removePending = (messageId) => {
    return (dispatch) => {
        dispatch(removePendingAction(messageId))
    }
}