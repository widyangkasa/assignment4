import { ADD_ROOM, CLEAR_PERISISTED_USER, UPDATE_ROOM } from "../../../../config/constants"

const initialState = {
}

const roomReducer = (state=initialState, action) => {
    switch (action.type){
        case UPDATE_ROOM: 
            return{
                ...state,
                [action.data.roomId] : {
                        ...state[action.data.roomId],
                        ...action.data
                    }
            }
        case ADD_ROOM:
            return{
                ...state,
                [action.data.roomId] : action.data.roomData
            }
        case CLEAR_PERISISTED_USER:
            return initialState
        default:
            return state
    }
}

export default roomReducer;
