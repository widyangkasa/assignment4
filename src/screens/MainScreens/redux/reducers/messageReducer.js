import { ADD_MESSAGE, CLEAR_PERISISTED_USER, SET_STATUS_DELIVERED, SET_STATUS_READ, UPDATE_MESSAGE } from "../../../../config/constants"

const initialState = {
}

const messageReducer = (state=initialState, action) => {
    switch (action.type){
        case UPDATE_MESSAGE: 
            return{
                ...state,
                [action.data.roomId] : {
                    ...state[action.data.roomId],
                    [action.data.messageData.id] : action.data.messageData
                }
            }
        case ADD_MESSAGE: 
            return{
                ...state,
                [action.data.roomId] : {
                    ...state[action.data.roomId],
                    [action.data.messageData.id] : action.data.messageData
                }
            }
        case SET_STATUS_DELIVERED: {
            return{
                ...state,
                [action.data.roomId] : {
                    ...state[action.data.roomId],
                    [action.data.messageId] : {
                        ...state[action.data.roomId][action.data.messageId],
                        status: 'delivered'
                    }
                }
            }
        }
        case SET_STATUS_READ:
            return {
                ...state,
                [action.data.roomId] : {
                    ...state[action.data.roomId],
                    [action.data.messageId] : {
                        ...state[action.data.roomId][action.data.messageId],
                        status: 'read'
                    }
                }
            }
        case CLEAR_PERISISTED_USER:
            return initialState
        default:
            return state
    }
}

export default messageReducer;
