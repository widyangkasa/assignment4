import { ADD_RUNNING, CLEAR_PERISISTED_USER, REMOVE_RUNNING } from "../../../../config/constants"

const initialState = {
}

const mRunningReducer = (state=initialState, action) => {
    switch (action.type){
        case ADD_RUNNING: 
            return{
                ...state,
                [action.data.messageId] : action.data.actionData 
            }
        case REMOVE_RUNNING:
            const {[action.data.messageId]:remove, ...rest } = state;
            return rest;
        case CLEAR_PERISISTED_USER:
            return initialState;
        default:
            return state;
    }
}

export default mRunningReducer;
