import { ADD_PENDING, CLEAR_PERISISTED_USER, REMOVE_PENDING, SET_PENDING_INUSE, SET_PENDING_NOTINUSE } from "../../../../config/constants"

const initialState = {
    inUse: false,
    data: {}
}

const mPendingReducer = (state=initialState, action) => {
    switch (action.type){
        case ADD_PENDING: 
            return{
                ...state,
                data : {
                    ...state.data,
                    [action.data.messageId] : action.data.actionData 
                }
            }
        case REMOVE_PENDING:
            const {[action.data.messageId]:remove, ...rest } = state.data;
            return {
                ...state,
                data: rest
            };
        case SET_PENDING_INUSE:
            return {
                ...state,
                inUse: true
            }
        case SET_PENDING_NOTINUSE:
            return {
                ...state,
                inUse: false
            }
        case CLEAR_PERISISTED_USER:
            return initialState;
        default:
            return state;
    }
}

export default mPendingReducer;
