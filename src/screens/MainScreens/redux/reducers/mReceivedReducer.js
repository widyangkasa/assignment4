import { ADD_RECEIVED, CLEAR_PERISISTED_USER, REMOVE_RECEIVED } from "../../../../config/constants"

const initialState = {
}

const mReceivedReducer = (state=initialState, action) => {
    switch (action.type){
        case ADD_RECEIVED: 
            return{
                ...state,
                [action.data.roomId] : {
                    ...state[action.data.roomId], 
                    [action.data.messageId]:{
                        id: action.data.messageId
                    }
                }
            }
        case REMOVE_RECEIVED:
            const {[action.data.messageId]:remove, ...rest } = state[action.data.roomId];
            return {
                ...state,
                [action.data.roomId] : {
                    ...rest
                }
            }
        case CLEAR_PERISISTED_USER:
            return initialState;
        default:
            return state;
    }
}

export default mReceivedReducer;
