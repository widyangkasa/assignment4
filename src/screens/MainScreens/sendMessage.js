import { addPendingMessage } from "./redux/actions/mPendingAction";
import firestore from '@react-native-firebase/firestore';
import axios from "axios";
import { removeRunning } from "./redux/actions/mRunningAction";
import { AUTHORIZATION_KEY } from "../../config/constants";
import { findReceiverIdbyRoomId } from "../../utils/generateId";

export const sendMessageToDB = (messageData, roomId, messageId) => {
    return async (dispatch, getState) => {
        const myId = messageData.sender;
        const receiverId = findReceiverIdbyRoomId(roomId, myId);
        try{
            const messageDataToDB = Object.assign({},messageData);
            messageDataToDB.status = 'sent';
            messageDataToDB.createdAt = firestore.FieldValue.serverTimestamp();
            // console.log(" room :", roomId, " userId1 :", myId, " userId2: ", receiverId)
            const roomDocRef = firestore().collection('rooms').doc(roomId);

            await firestore().runTransaction(function(transaction) {
                console.log("in transaction")
                return transaction.get(roomDocRef).then(function(roomDoc) {
                    if (!roomDoc.exists) {
                        console.log("room does not exist")
                        
                        // console.log(" room :", roomId, " userId1 :", myId, " userId2: ", receiverId)
                        var roomData = {
                            owner : [myId, receiverId],
                            lastMessage: {
                                id: messageId
                            },
                            roomId:roomId
                        }
                        roomData[myId]= {
                            'isTyping' : false,
                            'unRead': 0
                        },
                        roomData[receiverId]= {
                            'isTyping' : false,
                             'unRead': 1
                        },
                        
                        // console.log("roomdata", roomData)
                        transaction.set(roomDocRef, roomData)
                    }else{
                        const userChatsData = {
                            lastMessage: {
                                id: messageId
                            }
                        }   
                        userChatsData[receiverId] = {
                             'unRead': firestore.FieldValue.increment(1)
                        }
                        console.log("set increment");
                        transaction.set(roomDocRef,userChatsData, {merge:true});
                    }
                    console.log("set messages");
                    transaction.set(roomDocRef.collection('messages').doc(messageId),messageDataToDB, {merge: true});
                });
            }).then( async ()=>{
                dispatch(removeRunning(messageId))
                const snapshot = await firestore().collection('user').doc(receiverId).get();
                const notifData = {
                    roomId: roomId,
                    userId: myId
                }
                sendNotification(snapshot.data(),messageData.content, notifData);
            })            
        }catch(e){
            dispatch(removeRunning(messageId))
            dispatch(addPendingMessage(messageData, roomId))
            console.log(e)
        }
    }
} 

const sendNotification = (data, content, notifData) => {
    // console.log(data);
    if(data.token!=null && data.isLogin == true){
            const instance = axios.create({
                headers: {
                    Authorization: `key=${AUTHORIZATION_KEY}`,
                    'Content-Type': 'application/json'}
                }
              );
            instance.post('https://fcm.googleapis.com/fcm/send', {
                "to": data.token,
                "notification": {
                    "title": "You've got new Message",
                    "body": content,
                    "mutable_content": true,
                    "sound": "Tri-tone"
                    },
                "data": notifData
            })
            // .then(res=> {
            //     console.log("response notif", res)
            // }).catch(err=> {
            //     console.log("error notif", err)
            // })
    }
    
}
