import React, { Component } from 'react';
import { Text, Button, ScrollView} from 'react-native';
import { connect } from 'react-redux';
import { logout } from '../../../redux/actions/actions';


class HomeScreen extends Component{
    componentDidMount(){
        console.log("global state", JSON.stringify(this.props.global))
    }
    render() {
        return(
            <ScrollView>
                <Text>Welcome, {this.props.user.name} { this.props.user.id}</Text>
                <Text>{JSON.stringify(this.props.global)}</Text>
                <Button title="Logout" onPress={()=>this.props.logout(this.props.navigation, this.props.user.id)}/>
            </ScrollView>
            
        )
    }
}

const mapStateToProps=(state) => {
    return{
        user: state.user.data,
        global: state
    }
}

const mapDispatchToProps=(dispatch)=> {
    return{
        logout: (navigation, id)=> dispatch(logout(navigation, id))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);