import { SET_LOGIN_INPUT_VALUE,SET_REGISTER_INPUT_VALUE, RESET } from '../../../config/constants';

export const setLoginValueAction = (inputName, value, status) => {
    return {
        type: SET_LOGIN_INPUT_VALUE,
        data: {
            inputName,
            value,
            status
        }
    }
}

export const setRegisterValueAction = (inputName, value, status) => {
    return {
        type: SET_REGISTER_INPUT_VALUE,
        data: {
            inputName,
            value,
            status
        }
    }
}

export const resetAction = () => {
    return {
        type: RESET
    }
}