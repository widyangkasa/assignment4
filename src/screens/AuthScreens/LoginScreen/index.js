import React, {Component} from 'react';
import { StyleSheet, View, Text, Dimensions, Image} from 'react-native';

import NInputText from '../../../components/NInputText';
import NButton from '../../../components/NButton';
import { AuthStyle } from '../style';
import { Pattern } from '../../../utils/RegExp';

import { connect } from 'react-redux';
import { setLoginValueAction } from '../actions/actions';
import { login } from '../LoginScreen/LoginAction';

const {height, width} = Dimensions.get('window');
const pattern = Pattern;
let timer;
class LoginScreen extends Component{

    handleChangeText= (text, inputName) =>{
        clearTimeout(timer);
        let isValid = pattern[inputName].test(text);
        this.props.handleChangeText(inputName, text, 'loading');
        timer = setTimeout(()=>{
            this.props.handleChangeText(inputName, text, isValid?'valid':'invalid');
        },500 )  
    }

    goToRegister=(navigation)=>{
        navigation.navigate("Register");
    }


    render() {

        return(
            <View style={styles.container}>               
                <View style={styles.headerBg}/>
                <View style={styles.headerLogoWrapper} >
                    <Image source={require('../../../assets/logo_full.png')} style={styles.headerLogo}/>
                </View>
                <View style={styles.formWrapper}>
                    <Text style={styles.formHeader}>Login</Text>
                    {/* <Text style={{color: 'white'}}>{JSON.stringify(this.props.global)}</Text> */}
                    <View style={styles.formInput}>

                        {
                            loginInput.inputName.map((input, index)=>{
                                return (
                                    <NInputText 
                                    placeholder={input.placeholder} inputType={input.type}
                                    inputName={input.name} key={index} iconName={input.icon} 
                                    onChangeText={this.handleChangeText}
                                    colors="white" width={width*0.65}
                                    maxLength={input.maxLength}
                                    status= {this.props.data[input.name].status}
                                    value = {this.props.data[input.name].value}/>
                                )
                            })
                        }
                        {
                            this.props.error.isError? 
                            (<View><Text style={styles.errorMessage}>- {this.props.error.errorMessage}</Text></View>):
                            null
                        }
                        
                        <View style={styles.linkWrapper}><Text style={styles.linkDesc}>Forget password ? </Text><Text style={styles.link}>Reset Password</Text></View>
                        <View style={styles.linkWrapper}><Text style={styles.linkDesc}>Don't have account ? </Text><Text style={styles.link} onPress={()=> this.goToRegister(this.props.navigation)}>Sign Up</Text></View>
                    </View>
                    <View style={styles.submitBtnWrapper}>
                        {/* <TouchableOpacity style={styles.submitBtn}>
                            <Text style={{fontSize: 15}}>Login</Text>
                        </TouchableOpacity> */}
                        <NButton style={styles.submitBtn} 
                        onPress={()=> this.props.handleLogin(this.props.data, this.props.navigation)} 
                        loading={this.props.isLoading}>
                            <Text style={{fontSize: 15}}>Login</Text>
                        </NButton>
                        {/* <NButton onPress={} style={} title={} styleText={} loading={} /> */}
                    </View>
                </View>
                {this.props.isLoading? 
                    (<View style={styles.loadCover}></View>):
                    (null)
                }

            </View>
        )
    }
}

const loginInput = {
    inputName: [
        {
            placeholder: "Email", 
            type:"email", 
            name: "email", 
            icon: "envelope", 
            maxLength: 100
        }, 
        {
            placeholder:"password", 
            type:"password", 
            name:'password', 
            icon:"key", 
            maxLength: 20
        }
    ]
}

const mapStateToProps = (state) => {
    return {
        ...state.login,
        global: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeText : (inputName, value, status) => dispatch(setLoginValueAction(inputName, value, status)),
        handleLogin: (data, navigation) => dispatch(login(data, navigation))

    }
}

const styles=StyleSheet.create(AuthStyle(width, height));

export default LoginContainer = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

