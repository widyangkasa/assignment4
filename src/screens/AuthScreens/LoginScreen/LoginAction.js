import { IS_LOADING, FAIL_LOGIN } from '../../../config/constants';
import { validate } from '../validateInput';
import { ToastAndroid } from "react-native";
import {signIn} from '../../AuthScreens/SignIn';
import { persistUserAction } from '../../../redux/actions/actionCreator';

const setLoadingAction = isLoad => {
    return {
        type: IS_LOADING,
        data: isLoad
    }
}

const setFailLoginAction = (errorMessage) => {
    return {
        type: FAIL_LOGIN,
        data: {
            errorMessage
        }
    }
}

export const setTokenDevice = (token) => {
    return {
        type: FAIL_LOGIN,
        data: token
    }
}


const loginApi = (loginData)=> {
    console.log("hit loginapi block")
    const data = {
        email: loginData.email.value.toLowerCase(),
        password: loginData.password.value 
    }
    return new Promise(async (resolve, reject) => {
        try{
            const user = await signIn(data.email,data.password);
            console.log("user")
            resolve(user);
        }catch(e){
            reject(e);
        }
    })
}


export const login = (loginData, navigation) => {
    return (
        async (dispatch)=> {
            dispatch(setLoadingAction(true));
            try{
                await validate(loginData);
                const response = await loginApi(loginData);
                console.log("response" , response);
                dispatch(persistUserAction(response)); 
                navigation.replace("MainStack");
            }catch(err){
                if (err=='timeout'){
                    ToastAndroid.show("Connection timeout. please try again", ToastAndroid.SHORT);
                }else{
                    dispatch(setFailLoginAction(err.message))
                }
            }finally{
                dispatch(setLoadingAction(false));
            }
        }
    )
}


