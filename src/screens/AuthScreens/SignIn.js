import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';
import { errorMessage } from './firebaseAuthError';
import auth from '@react-native-firebase/auth';

export const signIn = async (email, password) => {
    try{
        const user = await signInUsingEmailAndPass(email, password);
        console.log(user);
        return user;
    }catch (e){
        console.log(e)
        throw e;
    }
}

const signInUsingEmailAndPass =async(email, password) => {
    console.log("sign in with email and pass")
    try{
        const response = await auth().signInWithEmailAndPassword(email, password);
        console.log(response)
        if (response.user.emailVerified==true){
            const userToken = await messaging().getToken();
            console.log("user Token", userToken);
            await firestore().collection('user').doc(response.user.uid).update({isLogin: true, token: userToken})
            console.log("done update token", userToken);
            const responsePublicData = await firestore().collection('user').doc(response.user.uid).get();
            console.log("get user data", responsePublicData);
            const responsePrivateData = await firestore().collection('userPrivate').doc(response.user.uid).get();
            console.log("get user private data", responsePrivateData);
            return Object.assign(responsePublicData.data(), responsePrivateData.data());
        }else {
            throw new Error(errorMessage("", "Email has not been verified. <View style={styles.link}> Resend? </View>"))
        }
    }catch (e){
        console.log(e)
        throw new Error(errorMessage(e.code, e.message));
    }
}
