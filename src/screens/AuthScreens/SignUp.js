import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import ResponseApi from '../../utils/ReponseApi';
import {errorMessage} from './firebaseAuthError';

export const signUp = async (userData) => {
    const data = userData;
    try{
        const authResponse = await createUserInAuth(data.private.email, data.private.password);
        console.log("auth response", authResponse);
        data.public['id'] = authResponse.user.uid;
        const isUserSaved = await saveUserToFirestore(data); //true or false
        const isEmailSent = await sendVerificationEmail();
        return new ResponseApi(authResponse.user, "Registration successful. We've sent link to verify your email address");
    }catch (e) {
        console.log(e)
        throw e;
    }
}


const sendVerificationEmail = async () => {
    var user = auth().currentUser;
    try{
        await user.sendEmailVerification();
        return true;
    }catch (e) {
        return false;
    }
}
//create user in auth
const createUserInAuth = async (email, password) => {
        try{
            const response = await auth().createUserWithEmailAndPassword(email, password);
            return response;
        }catch(e){
            throw new Error(errorMessage(e.code));
        }
}

//save new user to firestore after register to auth. 
const saveUserToFirestore = async (data) => {
    data.private['createdAt'] = firestore.FieldValue.serverTimestamp();
    try{
        console.log("user data to save", data);
        await firestore().collection('user').doc(data.public.id).set(data.public);
        await firestore().collection('userPrivate').doc(data.public.id).set(data.private);
        console.log("save user response");
        return true;
    }catch (e) {
        console.log("save user error. delete user on auth")
        const isDeleted = await deleteUserfromAuth();
        if (isDeleted){
            throw new Error(errorMessage(e.code));
        }else{
            throw new Error(errorMessage("","something wen't wrong. please contact us."));
        }
    }
}

//delete current user from auth, return true if success, false if failed
const deleteUserfromAuth = async () => {
    try{
        console.log(auth().currentUser)
        await auth().currentUser.delete();
        console.log("user deleted")
        return true
    }catch(e){
        console.log(e);
        return false;
    }
    
}