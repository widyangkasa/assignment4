import React, {Component} from 'react';
import { StyleSheet, View, Text, Dimensions, Image, TouchableOpacity} from 'react-native';
import NInputText from '../../../components/NInputText';
import NButton from '../../../components/NButton';
import {AuthStyle} from '../style';
import {isValidPattern} from '../../../utils/RegExp';

import {connect} from 'react-redux'
import { setRegisterValueAction } from '../actions/actions';
import { register } from '../RegisterScreen/RegisterAction';

const {height, width} = Dimensions.get('window');
let timer;
class RegisterScreen extends Component{    

    handleChangeText= (text, inputName) =>{
        clearTimeout(timer);
        let isValid = this.checkValidity(text, inputName);
        this.props.handleChangeText(inputName, text, 'loading');
        timer = setTimeout(()=>{
            this.props.handleChangeText(inputName, text, isValid?'valid':'invalid');
        },500 )  
    }

    checkValidity = (text, inputName) => {
        if (inputName!= 'confirmPassword'){
            return isValidPattern(text, inputName);
        }else {
            if(this.props.data.password.value === text) {
                console.log("equal password")
                return isValidPattern(text, inputName);
            }else {
                console.log("not equal to password")
                return false
            }
        }
    }

    goToLogin=(navigation)=>{
        navigation.navigate("Login");
    }

    render() {

        return(
            <View style={styles.container}>
                
                <View style={styles.headerBg}/>
                <View style={styles.headerLogoWrapper} >
                    <Image source={require('../../../assets/logo_full.png')} style={styles.headerLogo}/>
                </View>
                <View style={styles.formWrapper}>
                    <Text style={styles.formHeader}>Register</Text>
                    {/* <Text style={{color: 'white'}}>{JSON.stringify(this.props.global)}</Text> */}
                    <View style={styles.formInput}>

                        {
                            loginInput.inputName.map((input, index)=>{
                                return (
                                    <View key={index} >
                                        <NInputText 
                                            placeholder={input.placeholder} inputType = {input.type}
                                            inputName={input.name} iconName={input.icon} 
                                            onChangeText={this.handleChangeText}
                                            colors="white" width={width*0.65}
                                            maxLength={input.maxLength}
                                            status= {this.props.data[input.name].status}
                                            value={this.props.data[input.name].value}/>
                                    </View>
                                    
                                )
                            })
                        }
                        {
                            this.props.error.isError? 
                            (<View><Text style={styles.errorMessage}>- {this.props.error.errorMessage}</Text></View>):
                            null
                        }
                        <View style={styles.linkWrapper}>
                            <Text style={styles.linkDesc}>Already have an account ? </Text>
                            <Text style={styles.link} onPress={()=> this.goToLogin(this.props.navigation)}>Sign In</Text>
                        </View>
                    </View>
                    <View style={styles.submitBtnWrapper}>
                        <NButton style={styles.submitBtn} onPress={()=> this.props.handleRegister(this.props.data, this.props.navigation)} loading={this.props.isLoading}>
                            <Text style={{fontSize: 15}}>Register</Text>
                        </NButton>
                    </View>
                </View>
            </View>
        )
    }
}


const loginInput = {
    inputName: [
        {placeholder: "Full Name", type:"text", name: "fullName", icon: "user", maxLength: 50}, 
        {placeholder: "User Name", type:"text", name: "userName", icon: "user", maxLength: 50},
        {placeholder: "Email", name: "email", type:"email", icon: "envelope", maxLength: 100},
        {placeholder:"Password",type:"password", name:'password', icon:"key", maxLength: 20},
        {placeholder:"Confirm Password",type:"password", name:'confirmPassword', icon:"key", maxLength: 20}]
}

const mapStateToProps = (state) => {
    return {
        ...state.register,
        global: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeText : (inputName, value, status) => dispatch(setRegisterValueAction(inputName, value, status)),
        handleRegister: (data, navigation) => dispatch(register(data,navigation))
    }
}

const styles=StyleSheet.create(AuthStyle(width, height));

export default RegisterContainer = connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

