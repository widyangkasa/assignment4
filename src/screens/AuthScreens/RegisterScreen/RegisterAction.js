import { IS_LOADING, FAIL_REGISTER } from '../../../config/constants';
import { validate } from '../validateInput';
import { ToastAndroid } from "react-native";
import { resetAction, setLoginValueAction } from '../actions/actions';
import { signUp } from '../../AuthScreens/SignUp';

const setLoadingAction = isLoad => {
    return {
        type: IS_LOADING,
        data: isLoad
    }
}

const setFailRegisterAction = (errorMessage) => {
    return {
        type: FAIL_REGISTER,
        data: {
            errorMessage
        }
    }
}

const registerApi = (registerData)=> {
    const data = {
        public: {
            name: registerData.fullName.value,
            userName: registerData.userName.value,
            lastOnline: new Date(),
            isLogin: false,
            profilePic: "https://firebasestorage.googleapis.com/v0/b/chatapp-7a50a.appspot.com/o/profilePicture%2Fprofile_1_1604049039305.jpg?alt=media&token=23010ef9-0f71-4212-a866-cc237ca1688c"
        },
        private:{
            email: registerData.email.value.toLowerCase(),
            password: registerData.password.value,      
        }
    }
    return new Promise(async (resolve, reject) => {
        try{
            const response = await signUp(data);
            resolve(response);
        }catch (e){
            console.log("e",e);
            reject(e)
        }
    })
}


export const register = (registerData, navigation) => {
    console.log("in register block")
    return (
        async (dispatch)=> {
            dispatch(setLoadingAction(true));
            try{
                await validate(registerData);
                const resData = await registerApi(registerData);
                console.log(resData); 
                ToastAndroid.show(resData.getMessage(), ToastAndroid.SHORT);
                dispatch(resetAction());
                dispatch(setLoginValueAction("email", registerData.email.value, registerData.email.status));   
                navigation.navigate("Login");
            }catch (err) {
                if (err=='timeout'){
                    ToastAndroid.show("Connection timeout. please try again", ToastAndroid.SHORT);
                }else{
                    console.log(err.message)
                    dispatch(setFailRegisterAction(err.message))
                }
            }finally{
                dispatch(setLoadingAction(false));
            }            
        }
    )
}


