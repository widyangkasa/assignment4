import { SET_REGISTER_INPUT_VALUE, IS_LOADING, RESET, FAIL_REGISTER } from '../../../config/constants';

const initialState = {
    data:{
        email: {
            value: '',
            status: ''
        },
        password: {
            value: '',
            status: ''
        },
        fullName: {
            value: '',
            status: ''
        },
        userName: {
            value: '',
            status: ''
        },
        confirmPassword: {
            value: '',
            status: ''
        }
    },
    isLoading: false,
    error: {
        isError: false,
        errorMessage: ''
    }
}

export const registerReducer = (state = initialState, action) => {
    switch(action.type){
        case SET_REGISTER_INPUT_VALUE:
            return {
                ...state,
                data: {
                    ...state.data,
                    [action.data.inputName] : {
                        status: action.data.status,
                        value: action.data.value
                    }
                },
                error: {
                    isError: false,
                    errorMessage: ''
                } 
            }
        case RESET:
            return initialState;
        case IS_LOADING: 
            return {
                ...state,
                isLoading: action.data
            }
        case FAIL_REGISTER:
            return {
                ...state,
                error: {
                    isError: true,
                    errorMessage: action.data.errorMessage
                }
            }
        default: 
            return state;

    }
}