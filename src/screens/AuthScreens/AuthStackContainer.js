import React, { Component } from 'react';
import { AuthStack } from '../../Navigator';

class AuthStackContainer extends Component {

    render() {
        return (
            <AuthStack />
        );
    }
}

export default AuthStackContainer;