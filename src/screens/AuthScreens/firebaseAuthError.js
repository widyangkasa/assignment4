export const errorMessage = (code='', message='error update firebase') => {
    switch(code){
        case 'auth/email-already-in-use':
            return "Email already registered";
        case 'firestore/permission-denied':
            return "Permission denied"
        case 'auth/wrong-password':
            return "Email and Password not match";
        case 'auth/user-not-found':
            return "User not found";
        default:
            return message;
    }
}