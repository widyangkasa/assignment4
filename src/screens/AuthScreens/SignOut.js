import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { errorMessage } from './firebaseAuthError';

export const signOut = async (uid) => {
    try{
        await signOutUser(uid);
        console.log("signout");
    }catch (e){
        console.log(e)
        throw e;
    }
}

const signOutUser =async(uid) => {
    try{
        console.log(uid),
        console.log(auth().currentUser)
        await firestore().collection('user').doc(uid).update({isLogin: false})
        await auth().signOut();
    }catch (e){
        console.log(e)
        throw new Error(errorMessage(e.code, e.message));
    }
}
