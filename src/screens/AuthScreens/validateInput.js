import {isValidPattern, errInput} from '../../utils/RegExp';

export const validate = (data) =>{
    return new Promise((resolve, reject)=> {
        let isValid = true;
        for(let key in data) {
            console.log(key, data[key])
            if (key== 'confirmPassword'){
                if(data[key].value != data.password.value){
                    isValid = false;
                    reject({
                        message: errInput[key]
                    })
                }
            }
            else if(isValidPattern(data[key].value, key)==false){
                isValid= false;
                reject({
                    message: errInput[key]
                });
            }            
        }
        isValid? resolve("valid"): null
    })
}