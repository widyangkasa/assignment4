import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import {ProgressBar} from '@react-native-community/progress-bar-android';
import {connect} from 'react-redux';
import auth from '@react-native-firebase/auth';
class SplashScreen extends Component {

    state = {
        progress: 0
    }

    // forwardScreen= () => {
    //     let user = this.props.user
    //     if(Object.keys(user.data).length!=0){
    //         if(user.data.id!=0|| user.data.id!=''){
    //             this.props.navigation.replace("MainStack");
    //         }else{
    //             this.props.navigation.replace("AuthStack");
    //         }
    //     }else{
    //         this.props.navigation.replace("AuthStack");
    //     }
    // }

    onAuthStateChanged=(user)=> {
        if(user!= null && user.emailVerified ==true){
            this.props.navigation.replace("MainStack");
        }else{
            this.props.navigation.replace("AuthStack");
        }
      }
    


    //implement onAuthStateChange
      subscriber;
    componentDidMount(){
        const progress = this.state.progress;
        const newprogress = progress + 0.1;
        if(progress<1){
            setTimeout(() => {
                this.setState({
                    progress:newprogress
                })
            }, 200);
        }
    }

    componentDidUpdate() {
        const progress = this.state.progress;
        const newprogress = progress + 0.1;
        if(progress<1){
            setTimeout(() => {
                this.setState({
                    progress:newprogress
                })
            }, 200);
        }else{
            subscriber = auth().onAuthStateChanged((user)=> this.onAuthStateChanged(user));
        }
    }


    render() {
        return(
            <View style={styles.container}>
                <Image source={require('../../assets/logo1.png')} style={styles.logo} />
                <ProgressBar styleAttr="Horizontal" progress={this.state.progress} indeterminate={false} style={styles.loader}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#680500',
        justifyContent:'center',
        alignItems: 'center'
    },
    loader:{
        width:100
    }
})

const mapStateToProps= (state) => {
    return{
        user: state.user
    }
}

export default connect(mapStateToProps, null)(SplashScreen);
