import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux';
import { pStore, store } from './src/redux/store';
import { PersistGate } from 'redux-persist/integration/react';

import SplashScreen from './src/screens/SplashScreen';
import AuthStackContainer from './src/screens/AuthScreens/AuthStackContainer';
import MainStackContainer from './src/screens/MainScreens/MainStackContainer';

// const userDocument = firestore()
//   .collection('User')
//   .doc('bXbj7AUWWGC7LRyTrKLh').get().then((data)=>{console.log(data)});

class App extends Component {
  render() {
    const Stack = createStackNavigator();
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={pStore}>
          <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash" screenOptions={{headerShown: false}}>
              <Stack.Screen name="Splash" component={SplashScreen}/>
              <Stack.Screen name="AuthStack" component={AuthStackContainer}/>
              <Stack.Screen name="MainStack" component={MainStackContainer}/>
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    )
  }
}

export default App;
